extends Node

var Client_Port = 55555
var PlayerServer = WebSocketServer.new()

var Server_Port = 55556
var LobbyServer = WebSocketServer.new()

# List of all players with in a dictionary. Key = id Value = info
# Players:
# {
#  name:
#  id
# }
var players = {}
var lobbies = {}

func _ready():
	if OS.has_feature("lobby"):
		queue_free()
		return
	name = "NetworkManager"
	# Setup Networking for Clients
	PlayerServer.connect("client_connected", self, "_connected")
	PlayerServer.connect("client_disconnected", self, "_disconnected")
	PlayerServer.connect("data_received", self, "_on_client_data")
	var err = PlayerServer.listen(Client_Port)
	if err != OK:
		print("Unable to start client server")
		set_process(false)
	# Setup Networking for servers
	LobbyServer.connect("client_connected", self, "_connected")
	LobbyServer.connect("client_disconnected", self, "_disconnected")
	LobbyServer.connect("data_received", self, "_on_lobby_data")
	err = LobbyServer.listen(Server_Port)
	if err != OK:
		print("Unable to start lobby server")
		set_process(false)

func _process(delta):
	# Look for packets
	PlayerServer.poll()
	LobbyServer.poll()

#Client Management**************************************************************
func _player_connected(id):
	# Load Player from database
	pass

func _player_disconnected(id):
	# Save player info in database?
	players.erase(id)
	pass

# Messages come through with the following tree letter codes:
#	reg - register player The rest of the message is the dict/json of the player
#	lob - player is attempting to reserver a server to play on
func _on_player_data(id):
	var pkt = LobbyServer.get_peer(id).get_packet()
	var msg = pkt.get_string_from_utf8()
	match msg.substr(0,3):
		"reg":
			print("registering user:",msg.substr(3))
			register_player(msg.substr(3))
		"lob":
			create_lobby()

func create_lobby():
	OS.execute("./createLobby.sh",[],false)

func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	print("Adding Client with info:")
	print(info)
	players[id] = info

# Lobby Management**************************************************************
func _connected(id):
	print("%d connected" % [id])

func _on_lobby_data(id):
	# Print the received packet
	# and not get_packet directly when not using the MultiplayerAPI.
	var pkt = LobbyServer.get_peer(id).get_packet()
	print("Got data from client %d at ip %s: %s ... echoing" % [id,LobbyServer.get_peer_address(id), pkt.get_string_from_utf8()])
	LobbyServer.get_peer(id).put_packet(pkt)

func _disconnected(id, was_clean = false):
	lobbies.erase(id)
