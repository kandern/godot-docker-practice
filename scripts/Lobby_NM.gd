extends Node

# List of all players with in a dictionary. Key = id Value = info
# Players:
# {
#  name:
#  id
# }
var players = {}
var Server_Port = 55555
var Max_Players = 1000

var Manager_url = "ws://localhost:55556"
var ManagerConnection = WebSocketClient.new()
# Connect all functions
func _ready():
	if not OS.has_feature("lobby"):
		queue_free()
		return
	name = "NetworkManager"
	# Prepare for Client Connections
	var peer = NetworkedMultiplayerENet.new()
	peer.create_server(Server_Port, Max_Players)
	get_tree().network_peer = peer
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	get_tree().connect("connected_to_server", self, "_connected_ok")
	get_tree().connect("connection_failed", self, "_connected_fail")
	get_tree().connect("server_disconnected", self, "_server_disconnected")
	
	#Connect to the server Manager
	# Connect base signals to get notified of connection open, close, and errors.
	ManagerConnection.connect("connection_closed", self, "_closed")
	ManagerConnection.connect("connection_error", self, "_closed")
	ManagerConnection.connect("connection_established", self, "_connected_ok")
	# This signal is emitted when not using the Multiplayer API every time
	# a full packet is received.
	# Alternatively, you could check get_peer(1).get_available_packets() in a loop.
	ManagerConnection.connect("data_received", self, "_on_data")
	print("connecting")
	# Connect to server manager
	var err = ManagerConnection.connect_to_url(Manager_url)
	if err != OK:
		print("Unable to connect")
		set_process(false)
	else:
		print("connected?")

func _player_connected(id):
	pass

func _player_disconnected(id):
	pass

func _connected_ok(proto = ""):
	print("Connected to manager")
	print("Connected with protocol: ", proto)
	ManagerConnection.get_peer(1).put_packet("Test packet".to_utf8())

func _closed():
	print("Connection closed")
	
func _server_disconnected():
	pass # Server kicked us; show error and abort.

func _connected_fail():
	pass # Could not even connect to server; abort.

remote func register_connection(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
func _process(delta):
	# Call this in _process or _physics_process. Data transfer, and signals
	# emission will only happen when calling this function.
	ManagerConnection.poll()
